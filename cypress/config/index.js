const dtMass = require('./data-mass.json')
const ENV = Cypress.env('ENVIRONMENT')
export const credcesta = Cypress.env('credcesta')
export const mFacil = Cypress.env('mFacil')
export const mettaCard = Cypress.env('mettaCard')
export const apiCard = Cypress.env('apiCard')
// aqui, podemos incluir mais itens, conforme necessário utilizando o exemplo da linha acima "const ENV = Cypress.env('ENVIRONMENT')"

export const urls = {
  Credcesta: {
    login: `${credcesta}/vital-prime-api/v1/signin/unico`,
    // esqueciMinhaSenha: `${ehub}/esqueci-senha`,
  },
  // bbce: {
  //     home: `${bbce}`,
  //     login: `${bbce}/login`,
  //     Institucional: {
  //         quemSomos: `${bbce}/quem-somos`,
  //         governanca: `${bbce}/governanca`,
  //         autoregulacaoBBCE: `${bbce}/autoregulacao-bbce`,
  //         carreirasBBCE: `${bbce}/carreiras-bbce`,
  //     },
  //     Mercados: {
  //         mercadoLivre: `${bbce}/mercado-livre-energia`,
  //         derivativos: `${bbce}/derivativos`,
  //     },
  //     Ehub: {
  //         ehub: `${bbce}/ehub`,
  //     },
  //     SolucoesEServicos: {
  //         plataformaDerivativos: `${bbce}/plataforma-derivativos`,
  //         bbceLeiloes: `${bbce}/bbce-leiloes`,
  //         bbceConnect: `${bbce}/bbce-connect-integracao-mercado-livre`,
  //         bbceCurvaForward: `${bbce}/bbce-curva-forward`,
  //         bbceBoletaEletronica: `${bbce}/bbce-boleta-eletronica`,
  //     },
  //     Documentos: {
  //         cadastroBBCE: `${bbce}/cadastro`,
  //         comunicados: `${bbce}/comunicados`,
  //         documentos: `${bbce}/documentos`,
  //         participantesEmDerivativos: `${bbce}/participantes-em-derivativos`,
  //         validacaoDeContratos: `${bbce}/validacao-de-contratos`,
  //     },
  //     InsightEDados: {
  //         boletinsBBCE: `${bbce}/boletins-bbce`,
  //         dadosDiariosDerivativos: `${bbce}/dados-diarios`,
  //         ebooks: `${bbce}/ebooks`,
  //         noticias: `${bbce}/noticias`,
  //         webinarsBBCE: `${bbce}/webinarsbbce`,
  //     },
  //     Atendimento: {
  //         atendimento: `${bbce}/atendimento`,
  //     }
  // },
  // derivativos: {
  //     login: `${derivativos}/login`,
  //     recuperarSenha: `${derivativos}/recoverPassword`,
  // },
  // curvaForward: {
  //     home: `${curvaForward}`,
  //     login: `${curvaForward}Account/Login`,
  //     gerenciarPerfil: `${curvaForward}/Account/Manage`,
  //     esqueciMinhaSenha: `${curvaForward}/Account/ForgotPassword`,
  //     cadastro: `${curvaForward}/Account/Register`,
  // }
}


export const massCredcesta = dtMass[ENV].credcesta
export const massMfacil = dtMass[ENV].mFacil
export const massMetta = dtMass[ENV].credcesta
export const massApiCard = dtMass[ENV].apiCard

export const mass = {
  credcesta: {
    regular: massCredcesta.filter(x => x.type = 'regular').shift(),
    outroUsuario: massCredcesta.filter(x => x.type = 'outroUsuario').shift(),
    tokenLogado: massCredcesta.filter(x => x.type = 'tokenLogado').shift(),
    anonymousToken: massCredcesta.filter(x => x.type = 'anonymousToken').shift(),
  },
  mFacil: {
    regular: massMfacil.filter(x => x.type = 'regular').shift(),
    outroUsuario: massMfacil.filter(x => x.type = 'outroUsuario').shift(),
    tokenLogado: massMfacil.filter(x => x.type = 'tokenLogado').shift(),
    anonymousToken: massMfacil.filter(x => x.type = 'anonymousToken').shift(),
  },
  mettaCard: {
    regular: massMetta.filter(x => x.type = 'regular').shift(),
    outroUsuario: massMetta.filter(x => x.type = 'outroUsuario').shift(),
    tokenLogado: massMetta.filter(x => x.type = 'tokenLogado').shift(),
    anonymousToken: massMetta.filter(x => x.type = 'anonymousToken').shift(),
    testeSaque: massMetta.filter(x => x.type = 'testeSaque').shift(),
  }
}