import 'cypress-plugin-api'

describe('Teste de GET para obter informações do cartão', () => {

  beforeEach(() => {
    cy.anonymous_token()
    cy.signin_unico()
  })

  it('Obtendo informações do cartão', () => {

    const cpf = '03391661267' // Substitua isso pelo CPF que você deseja usar

    cy.task('getLoggedToken').then((loggedToken) => {
      cy.api({
        method: 'GET',
        url: 'https://hml-api-card.bancomaxima.com.br/card-api/v5/cards/03391661267',
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.39.0',
          'Accept': '*/*',
          //'Accept-Encoding': 'gzip, deflate, br',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
        },
      }).then((response) => {
        expect(response.status).to.eq(200) // Verifica se o status da resposta é 200
        expect(response.headers['content-type']).to.include('application/json') // Verifica se o Content-Type da resposta inclui 'application/json'

        // Validações para a resposta
        expect(response.body).to.have.property('cardsResult')
        expect(response.body.cardsResult[0]).to.have.property('documentNumber', cpf)
        // ...outras validações...

        // Armazena o UUID na variável global para uso posterior
        cy.task('setUuid', response.body.cardsResult[0].uuid)
      })
    })
  })
})