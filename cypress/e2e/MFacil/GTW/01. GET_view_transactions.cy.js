import 'cypress-plugin-api'

const mFacil = Cypress.env('mFacil')


describe('API Test', () => {

  beforeEach(() => {
    cy.signin_unico()
    cy.uuid_CPF()
  })

  it('Validação do histórico de transações', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${mFacil}/gtw-card-api/v5/cards/${uuid}/transactions-history`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
          qs: {
            'uuid': '3b5236da-0c8d-11ee-be56-0242ac120002' // Substitua 'valor' pelo valor desejado
          }
        })
          .then((response) => {
            console.log(response.body)
            expect(response.status).to.eq(200)

            // Validações adicionadas para o histórico de transações

            // Validação de Estrutura JSON
            expect(response.headers['content-type']).to.include('application/json')

            // Validação de Sucesso
            const jsonResponse = response.body
            expect(jsonResponse.sucesso).to.equal(true)

            // Validação de Registros Não Nulos
            expect(jsonResponse.registros.length).to.be.above(0)

            // Validação de Tipos de Dados
            const registros = jsonResponse.registros
            registros.forEach(registro => {
              expect(typeof registro.valorPrincipal).to.equal('string')
              expect(typeof registro.quantidadeParcelas).to.equal('string')
              expect(typeof registro.situacaoTransacao).to.equal('string')
              expect(typeof registro.estabelecimento).to.equal('string')
              expect(typeof registro.dataHoraCompra).to.equal('string')
              expect(typeof registro.cartaoVirtual).to.equal('boolean')
              expect(typeof registro.codigoAutorizacao).to.equal('string')
            })

            // Validação de Formato de Data e Hora
            registros.forEach(registro => {
              expect(registro.dataHoraCompra).to.match(/^\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}:\d{2},\d{6}$/)
            })

            // Validação de Cartão Virtual
            registros.forEach(registro => {
              expect(registro.cartaoVirtual).to.be.oneOf([true, false])
            })

            // Validação de Valor Principal Positivo
            registros.forEach(registro => {
              expect(parseFloat(registro.valorPrincipal)).to.be.above(0)
            })
          })
      })
    })
  })
})