import 'cypress-plugin-api'

const credCesta = Cypress.env('credCesta')

describe('Signin', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário
  let multiMatricula

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      // cy.credcesta_anonymous_token()
    })

    cy.readFile('cypress/config/data-mass.json').then((data) => {
      multiMatricula = data.hom.mettacard.multiMatricula
    })
  })

  it('Login com acesso bloqueado, pendente de habilitar o celular e login com sucesso', () => {
    cy.task('getAnonymousToken').then((anonymousToken) => {
      cy.api({
        method: 'POST',
        url: `${credCesta}/vital-prime-api/v1/signin/unico`, failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          //'Accept-Encoding': 'gzip, deflate, br',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
        },
        body: {
          'clientId': otherUserCreds.clientId, // Usa as credenciais do outro usuário
          'document': otherUserCreds.document,
          'password': otherUserCreds.pwd,
          'deviceId': otherUserCreds.deviceId
        }
      }).then((response) => {
        console.log(response.body)
        expect(response.status).to.eq(200)
        // Verifica se o acesso está bloqueado
        if (response.body.action === 'MAX_SIGNIN_ATTEMPTS') {
          expect(response.body).to.have.property('message', 'Atenção, acesso bloqueado por exceder tentativas permitidas com a senha incorreta. Por favor, crie uma nova senha.')
          expect(response.body).to.have.property('expiresIn', 0)
        } else if (response.body.action === 'UNKNOWN_DEVICE') {
          // Validação para quando o dispositivo é desconhecido
          expect(response.body).to.have.property('sessionId')
          expect(response.body).to.have.property('uuid')
          expect(response.body).to.have.property('message', 'Para sua segurança, por favor habilite seu celular para continuar a usar nosso aplicativo.')
          expect(response.body).to.have.property('expiresIn', 0)
        } else if (response.body.action === 'ACCOUNT_REPROVED') {
          // Validação para quando o cadastro não é aprovado
          expect(response.body).to.have.property('message', 'O seu cadastro não foi aprovado.\nAgradecemos seu interesse.')
          expect(response.body).to.have.property('expiresIn', 0)
        } else if (response.body.action === 'HOME') {
          // Validação para quando o usuário é direcionado para a HOME
          expect(response.body).to.have.property('accessToken')
          expect(response.body).to.have.property('expiresIn')
          expect(response.body).to.have.property('tokenType', 'Bearer')
        }
        // A linha abaixo só deve ser executada se o login for bem-sucedido
        if (response.body.accessToken) {
          cy.task('setLoggedToken', response.body.accessToken)
        }
      })

      it.only('Login com acesso bloqueado, pendente de habilitar o celular e login com sucesso', () => {
        cy.task('getAnonymousToken').then((anonymousToken) => {
          cy.api({
            method: 'POST',
            url: `${credCesta}/vital-prime-api/v1/signin/unico`, failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.38.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
            },
            body: {
              'clientId': multiMatricula.clientId, // Usa as credenciais do outro usuário
              'document': multiMatricula.document,
              'password': multiMatricula.pwd,
              'deviceId': multiMatricula.deviceId
            }
          }).then((response) => {
            console.log(response.body)
            expect(response.status).to.eq(200)
            // Verifica se o acesso está bloqueado
            if (response.body.action === 'MAX_SIGNIN_ATTEMPTS') {
              expect(response.body).to.have.property('message', 'Atenção, acesso bloqueado por exceder tentativas permitidas com a senha incorreta. Por favor, crie uma nova senha.')
              expect(response.body).to.have.property('expiresIn', 0)
            } else if (response.body.action === 'UNKNOWN_DEVICE') {
              // Validação para quando o dispositivo é desconhecido
              expect(response.body).to.have.property('sessionId')
              expect(response.body).to.have.property('uuid')
              expect(response.body).to.have.property('message', 'Para sua segurança, por favor habilite seu celular para continuar a usar nosso aplicativo.')
              expect(response.body).to.have.property('expiresIn', 0)
            } else if (response.body.action === 'ACCOUNT_REPROVED') {
              // Validação para quando o cadastro não é aprovado
              expect(response.body).to.have.property('message', 'O seu cadastro não foi aprovado.\nAgradecemos seu interesse.')
              expect(response.body).to.have.property('expiresIn', 0)
            } else if (response.body.action === 'HOME') {
              // Validação para quando o usuário é direcionado para a HOME
              expect(response.body).to.have.property('accessToken')
              expect(response.body).to.have.property('expiresIn')
              expect(response.body).to.have.property('tokenType', 'Bearer')
            }
            // A linha abaixo só deve ser executada se o login for bem-sucedido
            if (response.body.accessToken) {
              cy.task('setLoggedToken', response.body.accessToken)
            }
          })
        })
      })
    })
  })
})