import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')


describe('Visualizar transações', () => {

  beforeEach(() => {

    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação do histórico de transações Confirmadas e em Confirmação', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${mettaCard}/gtw-card-api/v5/cards/${uuid}/transactions-history`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
        })
          .then((response) => {
            console.log(response.body)
            expect(response.status).to.eq(200)

            // Validações adicionadas para o histórico de transações

            // Validação de Estrutura JSON
            expect(response.headers['content-type']).to.include('application/json')

            // Validação de Sucesso
            const jsonResponse = response.body
            expect(jsonResponse.sucesso).to.equal(true)

            // Validação de Registros Não Nulos
            expect(jsonResponse.registros.length).to.be.above(0)

            // Validação de Tipos de Dados
            const registros = jsonResponse.registros
            registros.forEach(registro => {
              expect(typeof registro.valorPrincipal).to.equal('string')
              expect(typeof registro.quantidadeParcelas).to.equal('string')
              expect(typeof registro.situacaoTransacao).to.equal('string')
              expect(typeof registro.estabelecimento).to.equal('string')
              expect(typeof registro.dataHoraCompra).to.equal('string')
              expect(typeof registro.cartaoVirtual).to.equal('boolean')
              expect(typeof registro.codigoAutorizacao).to.equal('string')
            })

            // Validação de Formato de Data e Hora
            registros.forEach(registro => {
              expect(registro.dataHoraCompra).to.match(/^\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}:\d{2},\d{6}$/)
            })

            // Validação de Cartão Virtual
            registros.forEach(registro => {
              expect(registro.cartaoVirtual).to.be.oneOf([true, false])
            })

            // Validação de Valor Principal Positivo
            registros.forEach(registro => {
              expect(parseFloat(registro.valorPrincipal)).to.be.above(0)
            })
          })
      })
    })
  })
})