import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Informações da conta', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação das informações completas da conta', () => {

    // const cpf = '41852271329' // Substitua isso pelo CPF que você deseja usar

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${mettaCard}/gtw-card-api/v5/cards/${uuid}/account/complete`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
        }).then((response) => {
          expect(response.status).to.eq(200)

          const body = response.body

          // Validações dos campos de nível superior
          expect(body).to.have.property('uuid').that.is.a('string')
          expect(body).to.have.property('org').that.is.a('string')
          expect(body).to.have.property('accountNumber').that.is.a('string')
          expect(body).to.have.property('documentNumber').that.is.a('string')
          expect(body).to.have.property('registrationNumber').that.is.a('string')
          expect(body).to.have.property('embossName').that.is.a('string')
          expect(body).to.have.property('accountStatusDescription').that.is.a('string')
          expect(body).to.have.property('accountStatus').that.is.a('string')
          expect(body).to.have.property('birthDate').that.is.a('string')
          expect(body).to.have.property('logo').that.is.a('string')
          expect(body).to.have.property('ddd').that.is.a('string')
          expect(body).to.have.property('phoneNumber').that.is.a('string')
          expect(body).to.have.property('dddCellPhone').that.is.a('string')
          expect(body).to.have.property('cellPhone').that.is.a('string')
          expect(body).to.have.property('dddCellPhoneAdditional').that.is.a('string')
          expect(body).to.have.property('cellPhoneAdditional').that.is.a('string')
          expect(body).to.have.property('valueClosedInvoiceAmount').that.is.a('string')
          expect(body).to.have.property('minimumPaymentPayment').that.is.a('string')
          expect(body).to.have.property('motherName').that.is.a('string')
          expect(body).to.have.property('gender').that.is.a('string')
          expect(body).to.have.property('balanceBill').that.is.a('string')
        })
      })
    })
  })
})