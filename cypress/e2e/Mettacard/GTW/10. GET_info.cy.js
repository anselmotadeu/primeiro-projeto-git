import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Informações do cartão', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Obtendo informações do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${mettaCard}/gtw-card-api/v5/cards/${uuid}/info`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos de nível superior
            expect(body).to.have.property('uuid').that.is.a('string')
            expect(body).to.have.property('embossing').that.is.a('string')
            expect(body).to.have.property('embossName').that.is.a('string')
            expect(body).to.have.property('expirationDate').that.is.a('string')
            expect(body).to.have.property('last4Digits').that.is.a('string')
            expect(body).to.have.property('logoNumber').that.is.a('string')
            expect(body).to.have.property('status').that.is.a('string')
            expect(body).to.have.property('limitNac').that.is.a('number')
            expect(body).to.have.property('limitNacAvailable').that.is.a('number')
            expect(body).to.have.property('limitNacUsed').that.is.a('number')
            expect(body).to.have.property('blockCode').that.is.a('string')
            expect(body).to.have.property('cartaoDevolvido').that.is.a('boolean')
            expect(body).to.have.property('transactions').that.is.an('array')

            // Validações para cada item no array 'transactions'
            body.transactions.forEach((transaction) => {
              expect(transaction).to.have.property('date').that.is.a('string')
              expect(transaction).to.have.property('numCard').that.is.a('string')
              expect(transaction).to.have.property('nameBearer').that.is.a('string')
              expect(transaction).to.have.property('numParcelUpTo').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionInternationalDolllar').that.is.a('string')
              expect(transaction).to.have.property('numReference').that.is.a('string')
              expect(transaction).to.have.property('codTxn').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionSum').that.is.a('string')
              expect(transaction).to.have.property('description').that.is.a('string')
              expect(transaction).to.have.property('flagTechnology').that.is.a('string')
              expect(transaction).to.have.property('nameGroup').that.is.a('string')
              expect(transaction).to.have.property('typetransaction').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionNational').that.is.a('string')
              expect(transaction).to.have.property('value').that.is.a('number')
              expect(transaction).to.have.property('codBranchActivity').that.is.a('string')
              expect(transaction).to.have.property('codEstablishment').that.is.a('string')
              expect(transaction).to.have.property('valueQuotationDollar').that.is.a('string')
              expect(transaction).to.have.property('numPlan').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionInternational').that.is.a('string')
              expect(transaction).to.have.property('numRefAcquirer').that.is.a('string')
              expect(transaction).to.have.property('flagParcelInterest').that.is.a('string')
              expect(transaction).to.have.property('numParcelOf').that.is.a('string')
              expect(transaction).to.have.property('abbreviationGroup').that.is.a('string')
              expect(transaction).to.have.property('type').that.is.a('string')
            })
          })
        })
      })
    })
  })
})