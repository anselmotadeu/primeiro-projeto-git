import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Informações do cartão através do CPF', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
  })

  it('Obtendo informações do cartão', () => {

    const cpf = '41852271329' // Substitua isso pelo CPF que você deseja usar

    cy.task('getLoggedToken').then((loggedToken) => {
      cy.api({
        method: 'GET',
        url: `${mettaCard}/gtw-card-api/v5/cards/${cpf}`,
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.39.0',
          'Accept': '*/*',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
        },
      }).then((response) => {
        expect(response.status).to.eq(200)

        const body = response.body
        expect(body).to.have.property('cardsResult').that.is.an('array')

        // Validações para cada item no array 'cardsResult'
        body.cardsResult.forEach((card) => {
          expect(card).to.have.property('uuid').that.is.a('string')
          expect(card).to.have.property('documentNumber').that.is.a('string')
          expect(card).to.have.property('numOrg').that.is.a('string')
          expect(card).to.have.property('numLogo').that.is.a('number')
          expect(card).to.have.property('last4Digits').that.is.a('string')
          expect(card).to.have.property('last4DigitsVirtual').that.is.a('string')
          expect(card).to.have.property('status').that.is.a('string')

          // Armazena o UUID e last4Digits na variável global para uso posterior
          const uuid = response.body.cardsResult[0].uuid
          const last4Digits = response.body.cardsResult[0].last4Digits
          cy.task('setUuid', uuid)
          cy.task('setLast4Digits', last4Digits)

        })
      })
    })
  })
})