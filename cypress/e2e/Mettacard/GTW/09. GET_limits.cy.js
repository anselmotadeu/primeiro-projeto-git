import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Limites', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação dos limites do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${mettaCard}/gtw-card-api/v5/cards/${uuid}/limits`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            if (response.status === 200) {
              const body = response.body

              // Validações dos campos de nível superior
              expect(body).to.have.property('creditLimitCashNacAvailable').that.is.a('number')
              expect(body).to.have.property('creditLimitCashInter').that.is.a('number')
              expect(body).to.have.property('creditLimitNacAvailable').that.is.a('number')
              expect(body).to.have.property('creditLimitNac').that.is.a('number')
              expect(body).to.have.property('creditLimitNacUsed').that.is.a('number')
              expect(body).to.have.property('creditLimitCashNac').that.is.a('number')
              expect(body).to.have.property('creditLimitInterAvailable').that.is.a('number')
              expect(body).to.have.property('creditLimitInter').that.is.a('number')
              expect(body).to.have.property('creditLimitCashInterAvailable').that.is.a('number')
            } else if (response.status === 500) {
              const body = response.body

              // Validações dos campos de erro
              expect(body).to.have.property('status').that.is.a('number').and.eq(400)
              expect(body).to.have.property('title').that.is.a('string').and.eq('ValidationError')
              expect(body).to.have.property('result').that.is.null
              expect(body).to.have.property('errors').that.is.an('object')

              // Validações específicas do campo 'errors'
              expect(body.errors).to.have.property('last4Digits').that.is.an('array')
              body.errors.last4Digits.forEach((error) => {
                expect(error).to.be.a('string').and.eq('last4Digits é obrigatório')
              })
            } else {
              throw new Error(`Unexpected status code: ${response.status}`)
            }
          })
        })
      })
    })
  })
})