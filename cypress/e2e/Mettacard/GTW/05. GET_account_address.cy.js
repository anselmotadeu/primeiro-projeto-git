import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')


describe('Endereço da conta', () => {

  beforeEach(() => {

    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação do endereço da conta', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${mettaCard}/gtw-card-api/v1/card/${uuid}/account/address`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
        }).then((response) => {
          expect(response.status).to.eq(200)

          const body = response.body

          // Validações dos campos de nível superior
          expect(body).to.have.property('zipCode').that.is.a('string')
          expect(body).to.have.property('address').that.is.a('string')
          expect(body).to.have.property('addressNumber').that.is.a('string')
          expect(body).to.have.property('addressComplement').that.is.a('string')
          expect(body).to.have.property('district').that.is.a('string')
          expect(body).to.have.property('city').that.is.a('string')
          expect(body).to.have.property('state').that.is.a('string')
        })
      })
    })
  })
})