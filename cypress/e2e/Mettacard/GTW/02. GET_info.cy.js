import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')


describe('Informações do cartão', () => {

  beforeEach(() => {

    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação das informações do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${mettaCard}/gtw-card-api/v1/card/${uuid}/info`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
        }).then((response) => {
          expect(response.status).to.eq(200)

          const body = response.body

          // Validações dos campos de nível superior
          expect(body).to.have.property('last4Digits').that.is.a('string')
          expect(body).to.have.property('status').that.is.a('string')
          expect(body).to.have.property('limitNac').that.is.a('number')
          expect(body).to.have.property('limitNacAvailable').that.is.a('number')
          expect(body).to.have.property('limitNacUsed').that.is.a('number')
          expect(body).to.have.property('actions').that.is.an('array')
          expect(body).to.have.property('cartaoDevolvido').that.is.a('boolean')

          // Validações para cada item no array 'actions'
          body.actions.forEach((action) => {
            expect(action).to.be.a('string')
          })
        })
      })
    })
  })
})