import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Bloqueio do cartão', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Atualização de bloqueio do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'PUT',
          url: `${apiCard}/card-api/v1/cards/${uuid}/account/block`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
          body: {
            last4digits: '' // Certifique-se de enviar o corpo da requisição
          }
        }).then((response) => {
          expect(response.status).to.eq(200)

          const body = response.body

          // Validações dos campos de nível superior
          expect(body).to.have.property('returnCode').that.is.a('string')
          expect(body).to.have.property('returnDescription').that.is.a('string')
        })
      })
    })
  })
})