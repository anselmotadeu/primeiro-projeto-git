import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Fatura do cartão', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação das informações contidas na fatura do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${apiCard}/card-api/v1/cards/${uuid}/invoice/transactions`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos de nível superior
            expect(body).to.have.property('creditLimitInternationalValue').that.is.a('string')
            expect(body).to.have.property('CETMonthlyRevolvingCreditCash').that.is.a('string')
            expect(body).to.have.property('totalExpensesValue').that.is.a('string')
            expect(body).to.have.property('financialBurdenNextInvoiceCashValue').that.is.a('string')
            expect(body).to.have.property('dueDate').that.is.a('string')
            expect(body).to.have.property('previousInvoiceValue').that.is.a('string')
            expect(body).to.have.property('bsInstallmentQuantity').that.is.a('number')
            expect(body).to.have.property('creditLimitNationalValue').that.is.a('string')
            expect(body).to.have.property('bsBalanceNotParcel').that.is.a('string')
            expect(body).to.have.property('requiredPaymentFlag').that.is.a('string')
            expect(body).to.have.property('bsPaymentMinNotParcel').that.is.a('string')
            expect(body).to.have.property('CETAnnualRevolvingCredit').that.is.a('string')
            expect(body).to.have.property('quotationDollarValue').that.is.a('string')
            expect(body).to.have.property('nextDueDate').that.is.a('string')
            expect(body).to.have.property('CETRevolvingPrcAnnual').that.is.a('string')
            expect(body).to.have.property('expenditureInternationalValue').that.is.a('string')
            expect(body).to.have.property('registrationDate').that.is.a('string')
            expect(body).to.have.property('totalNationalCashValue').that.is.a('string')
            expect(body).to.have.property('totalInvoiceValue').that.is.a('string')
            expect(body).to.have.property('financialBurdenCashValue').that.is.a('string')
            expect(body).to.have.property('dateCut').that.is.a('string')
            expect(body).to.have.property('totalDebitInternationalValue').that.is.a('string')
            expect(body).to.have.property('totalCreditValue').that.is.a('string')
            expect(body).to.have.property('digitableLine').that.is.a('string')
            expect(body).to.have.property('bsFinancedValue').that.is.a('string')
            expect(body).to.have.property('financialBurdenNextInvoiceValue').that.is.a('string')
            expect(body).to.have.property('CETMonthlyRevolvingCredit').that.is.a('string')
            expect(body).to.have.property('totalExpensesRevolvingValue').that.is.a('string')
            expect(body).to.have.property('CETAnnualRevolvingCreditCash').that.is.a('string')
            expect(body).to.have.property('paidNotIntegralValue').that.is.a('string')
            expect(body).to.have.property('creditInternationalValue').that.is.a('string')
            expect(body).to.have.property('bestPurchaseDay').that.is.a('string')
            expect(body).to.have.property('totalExpensesInternationalValue').that.is.a('string')
            expect(body).to.have.property('totalDebitNationalValue').that.is.a('string')
            expect(body).to.have.property('withdrawalCashLimitInternationalValue').that.is.a('string')
            expect(body).to.have.property('totalCreditInternationalValue').that.is.a('string')
            expect(body).to.have.property('bsProratedTotal').that.is.a('string')
            expect(body).to.have.property('totalTariffValue').that.is.a('string')
            expect(body).to.have.property('totalCreditNationalValue').that.is.a('string')
            expect(body).to.have.property('financialBurdenValue').that.is.a('string')
            expect(body).to.have.property('CETRevolvingPrcMonthly').that.is.a('string')
            expect(body).to.have.property('withdrawalCashLimitNationalValue').that.is.a('string')
            expect(body).to.have.property('totalPaymentValue').that.is.a('string')
            expect(body).to.have.property('totalDebitValue').that.is.a('string')
            expect(body).to.have.property('bsTaxInterestMonth').that.is.a('string')
            expect(body).to.have.property('bsInstallmentValue').that.is.a('string')
            expect(body).to.have.property('totalExpensesProratedValue').that.is.a('string')
            expect(body).to.have.property('paidIntegralValue').that.is.a('string')
            expect(body).to.have.property('code').that.is.a('string')
            expect(body).to.have.property('description').that.is.a('string')

            // Validações para cada item no array 'transaction'
            body.transaction.forEach((transaction) => {
              expect(transaction).to.have.property('date').that.is.a('string')
              expect(transaction).to.have.property('numCard').that.is.a('string')
              expect(transaction).to.have.property('nameBearer').that.is.a('string')
              expect(transaction).to.have.property('numParcelUpTo').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionInternationalDolllar').that.is.a('string')
              expect(transaction).to.have.property('numReference').that.is.a('string')
              expect(transaction).to.have.property('codTxn').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionSum').that.is.a('string')
              expect(transaction).to.have.property('description').that.is.a('string')
              expect(transaction).to.have.property('flagTechnology').that.is.a('string')
              expect(transaction).to.have.property('nameGroup').that.is.a('string')
              expect(transaction).to.have.property('typetransaction').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionNational').that.is.a('string')
              expect(transaction).to.have.property('value').that.is.a('number')
              expect(transaction).to.have.property('codBranchActivity').that.is.a('string')
              expect(transaction).to.have.property('codEstablishment').that.is.a('string')
              expect(transaction).to.have.property('valueQuotationDollar').that.is.a('string')
              expect(transaction).to.have.property('numPlan').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionInternational').that.is.a('string')
              expect(transaction).to.have.property('numRefAcquirer').that.is.a('string')
              expect(transaction).to.have.property('flagParcelInterest').that.is.a('string')
              expect(transaction).to.have.property('numParcelOf').that.is.a('string')
              expect(transaction).to.have.property('abbreviationGroup').that.is.a('string')
              expect(transaction).to.have.property('type').that.is.a('string')
            })
          })
        })
      })
    })
  })
})