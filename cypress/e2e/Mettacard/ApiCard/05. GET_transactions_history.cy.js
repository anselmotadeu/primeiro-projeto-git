import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Transações do cartão', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Obtendo histórico de transações do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${apiCard}/card-api/v5/cards/${uuid}/transactions-history`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos de nível superior
            expect(body).to.have.property('returnCode').that.is.a('string')
            expect(body).to.have.property('returnMessage').that.is.a('string')
            expect(body).to.have.property('register').that.is.an('array')

            // Validações para cada item no array 'register'
            body.register.forEach((item) => {
              expect(item).to.have.property('cashFactorInternational').that.is.a('string')
              expect(item).to.have.property('cauculationMethod').that.is.a('string')
              expect(item).to.have.property('establishmentCode').that.is.a('string')
              expect(item).to.have.property('tmsInclH').that.is.a('string')
              expect(item).to.have.property('numReference').that.is.a('string')
              expect(item).to.have.property('dolarTransactionValue').that.is.a('string')
              expect(item).to.have.property('operatorCode').that.is.a('string')
              expect(item).to.have.property('purchaseType').that.is.a('string')
              expect(item).to.have.property('descResponseTransaction').that.is.a('string')
              expect(item).to.have.property('mccCode').that.is.a('string')
              expect(item).to.have.property('CDCParceldFlag').that.is.a('string')
              expect(item).to.have.property('parselValue').that.is.a('string')
              expect(item).to.have.property('homeContryCode').that.is.a('string')
              expect(item).to.have.property('dolarQuotationValue').that.is.a('string')
              expect(item).to.have.property('authorizationExceptionFlag').that.is.a('string')
              expect(item).to.have.property('coinCode').that.is.a('string')
              expect(item).to.have.property('interstTotalValue').that.is.a('string')
              expect(item).to.have.property('establishmentName').that.is.a('string')
              expect(item).to.have.property('iterestTransactionValue').that.is.a('string')
              expect(item).to.have.property('posEntry').that.is.a('string')
              expect(item).to.have.property('qtdDays').that.is.a('string')
              expect(item).to.have.property('transactionResponseCode').that.is.a('string')
              expect(item).to.have.property('transactionType').that.is.a('string')
              expect(item).to.have.property('qtdParcel').that.is.a('string')
              expect(item).to.have.property('coinTransactionValue').that.is.a('string')
              expect(item).to.have.property('mainValue').that.is.a('string')
              expect(item).to.have.property('coinName').that.is.a('string')
              expect(item).to.have.property('descDrop').that.is.a('string')
              expect(item).to.have.property('numMachine').that.is.a('string')
              expect(item).to.have.property('interestTax').that.is.a('string')
              expect(item).to.have.property('numAuthorization').that.is.a('string')
            })
          })
        })
      })
    })
  })
})