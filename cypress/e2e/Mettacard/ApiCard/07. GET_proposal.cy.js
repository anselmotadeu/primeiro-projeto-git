import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Proposta', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Obtendo informações dde proposta do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${apiCard}/card-api/v2/cards/proposal/${uuid}`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
          },
        }).then((response) => {
          expect(response.status).to.eq(200)

          const body = response.body

          // Validações dos campos de nível superior
          expect(body).to.have.property('code').that.is.a('string')
          expect(body).to.have.property('description').that.is.a('string')
          expect(body).to.have.property('proposals').that.is.an('array')

          // Validações para cada item no array 'proposals'
          body.proposals.forEach((proposalArray) => {
            expect(proposalArray).to.be.an('array')
            proposalArray.forEach((proposal) => {
              expect(proposal).to.have.property('dateProposal').that.is.a('string')
              expect(proposal).to.have.property('validThru').that.is.a('string')
              expect(proposal).to.have.property('mainAccount').that.is.a('string')
              expect(proposal).to.have.property('binDebitCard').that.is.a('string')
              expect(proposal).to.have.property('codeProposal').that.is.a('string')
              expect(proposal).to.have.property('relationCode').that.is.a('string')
              expect(proposal).to.have.property('binCard').that.is.a('string')
              expect(proposal).to.have.property('last4DigitsDebit').that.is.a('string')
              expect(proposal).to.have.property('uuid').that.is.a('string')
              expect(proposal).to.have.property('memberSince').that.is.a('string')
              expect(proposal).to.have.property('relationOrgNumber').that.is.a('string')
              expect(proposal).to.have.property('reservedAmtAcct').that.is.a('string')
              expect(proposal).to.have.property('creditLimitAccount').that.is.a('string')
              expect(proposal).to.have.property('taxid').that.is.a('string')
              expect(proposal).to.have.property('orgAccountNumber').that.is.a('number')
              expect(proposal).to.have.property('cardStatus').that.is.a('string')
              expect(proposal).to.have.property('last4Digits').that.is.a('string')
              expect(proposal).to.have.property('status').that.is.a('string')
            })
          })
        })

      })
    })
  })
})