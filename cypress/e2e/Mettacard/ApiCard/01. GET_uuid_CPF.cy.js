import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Informações do cartão através do CPF', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
  })

  it('Obtendo informações do cartão', () => {

    const cpf = '41852271329' // Substitua isso pelo CPF que você deseja usar

    cy.task('getLoggedToken').then((loggedToken) => {
      cy.api({
        method: 'GET',
        url: `${apiCard}/card-api/v5/cards/${cpf}`,
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.39.0',
          'Accept': '*/*',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
        },
      }).then((response) => {
        expect(response.status).to.eq(200) // Verifica se o status da resposta é 200
        expect(response.headers['content-type']).to.include('application/json') // Verifica se o Content-Type da resposta inclui 'application/json'

        // Armazena o UUID e last4Digits na variável global para uso posterior
        const uuid = response.body.cardsResult[0].uuid
        const last4Digits = response.body.cardsResult[0].last4Digits
        cy.task('setUuid', uuid)
        cy.task('setLast4Digits', last4Digits)

      })
    })
  })
})