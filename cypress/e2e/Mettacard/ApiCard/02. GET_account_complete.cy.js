import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Informações da conta', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação das informações completas da conta', () => {

    // const cpf = '41852271329' // Substitua isso pelo CPF que você deseja usar

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.api({
          method: 'GET',
          url: `${apiCard}/card-api/v5/cards/${uuid}/account/complete`,
          failOnStatusCode: false,
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.39.0',
            'Accept': '*/*',
            //'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
          },
        }).then((response) => {
          expect(response.status).to.eq(200)

          const body = response.body

          // Validações dos campos
          expect(body).to.have.property('zipCode').that.is.a('string')
          expect(body).to.have.property('fatherName').that.is.a('string')
          expect(body).to.have.property('creditLimitCashInter').that.is.a('string')
          expect(body).to.have.property('flagTypeBilling').that.is.a('string')
          expect(body).to.have.property('mail').that.is.a('string')
          expect(body).to.have.property('flagContact').that.is.a('string')
          expect(body).to.have.property('documentNumber').that.is.a('string')
          expect(body).to.have.property('internationalPurchaseValue').that.is.a('string')
          expect(body).to.have.property('dddCellPhone').that.is.a('string')
          expect(body).to.have.property('dateLastPayment').that.is.a('string')
          expect(body).to.have.property('clientNameHolder').that.is.a('string')
          expect(body).to.have.property('dayDue').that.is.a('string')
          expect(body).to.have.property('validThruCard').that.is.a('string')
          expect(body).to.have.property('uuid').that.is.a('string')
          expect(body).to.have.property('interestCashNextPeriod').that.is.a('string')
          expect(body).to.have.property('returnCode').that.is.a('string')
          expect(body).to.have.property('blockCodeCard').that.is.a('string')
          expect(body).to.have.property('cellPhoneAdditional').that.is.a('string')
          expect(body).to.have.property('dateNextCut').that.is.a('string')
          expect(body).to.have.property('dateCancel').that.is.a('string')
          expect(body).to.have.property('dueDateLastInvoice').that.is.a('string')
          expect(body).to.have.property('nextDueDate').that.is.a('string')
          expect(body).to.have.property('flagInstallmentPurchase').that.is.a('string')
          expect(body).to.have.property('descriptionPersonType').that.is.a('string')
          expect(body).to.have.property('creditLimitInter').that.is.a('string')
          expect(body).to.have.property('logo').that.is.a('string')
          expect(body).to.have.property('embossName').that.is.a('string')
          expect(body).to.have.property('state').that.is.a('string')
          expect(body).to.have.property('balanceBill').that.is.a('string')
          expect(body).to.have.property('creditLimitNac').that.is.a('string')
          expect(body).to.have.property('genderType').that.is.a('string')
          expect(body).to.have.property('addressComplement').that.is.a('string')
          expect(body).to.have.property('currentCutDay').that.is.a('string')
          expect(body).to.have.property('blockDateAccount').that.is.a('string')
          expect(body).to.have.property('dddCellPhoneAdditional').that.is.a('string')
          expect(body).to.have.property('motherName').that.is.a('string')
          expect(body).to.have.property('accountOpeningDate').that.is.a('string')
          expect(body).to.have.property('contractNumber').that.is.a('string')
          expect(body).to.have.property('chashbill').that.is.a('string')
          expect(body).to.have.property('installmentPurchaseQuantity').that.is.a('number')
          expect(body).to.have.property('embossType').that.is.a('string')
          expect(body).to.have.property('phoneNumber').that.is.a('string')
          expect(body).to.have.property('flagCardAccount').that.is.a('string')
          expect(body).to.have.property('district').that.is.a('string')
          expect(body).to.have.property('addressNumber').that.is.a('string')
          expect(body).to.have.property('nextInvoiceDate').that.is.a('string')
          expect(body).to.have.property('maritalStatus').that.is.a('string')
          expect(body).to.have.property('last4Digits').that.is.a('string')
          expect(body).to.have.property('typeCard').that.is.a('string')
          expect(body).to.have.property('gender').that.is.a('string')
          expect(body).to.have.property('city').that.is.a('string')
          expect(body).to.have.property('clientName').that.is.a('string')
          expect(body).to.have.property('accountStatusDescription').that.is.a('string')
          expect(body).to.have.property('minimumPaymentPayment').that.is.a('string')
          expect(body).to.have.property('accountStatus').that.is.a('string')
          expect(body).to.have.property('dateLastChangeDue').that.is.a('string')
          expect(body).to.have.property('blockDateAccountTwo').that.is.a('string')
          expect(body).to.have.property('personType').that.is.a('string')
          expect(body).to.have.property('blockCodeCardTwo').that.is.a('string')
          expect(body).to.have.property('interestRateNextPeriod').that.is.a('string')
          expect(body).to.have.property('embossTypeDescription').that.is.a('string')
          expect(body).to.have.property('dateGenerationCard').that.is.a('string')
          expect(body).to.have.property('phoneNumberWork').that.is.a('string')
          expect(body).to.have.property('address').that.is.a('string')
          expect(body).to.have.property('valueLastPayment').that.is.a('string')
          expect(body).to.have.property('orgDescription').that.is.a('string')
          expect(body).to.have.property('creditLimitCashNac').that.is.a('string')
          expect(body).to.have.property('org').that.is.a('string')
          expect(body).to.have.property('ddd').that.is.a('string')
          expect(body).to.have.property('valueClosedInvoiceAmount').that.is.a('string')
          expect(body).to.have.property('returnDescription').that.is.a('string')
          expect(body).to.have.property('accountNumber').that.is.a('string')
          expect(body).to.have.property('birthDate').that.is.a('string')
          expect(body).to.have.property('blockCodeAccountTwo').that.is.a('string')
          expect(body).to.have.property('rmcValue').that.is.a('string')
          expect(body).to.have.property('flagBlockCard').that.is.a('string')
          expect(body).to.have.property('registrationNumber').that.is.a('string')
          expect(body).to.have.property('chiprasNumber').that.is.a('string')
          expect(body).to.have.property('dateLastChangeLimit').that.is.a('string')
          expect(body).to.have.property('blockCodeAccount').that.is.a('string')
          expect(body).to.have.property('cellPhone').that.is.a('string')
        })
      })
    })
  })
})