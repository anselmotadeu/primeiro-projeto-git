import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Detalhes do cartão', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Obtendo detalhes do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${apiCard}/card-api/v5/cards/${uuid}/details`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos
            expect(body).to.have.property('cardNumber').that.is.a('string')
            expect(body).to.have.property('embossName').that.is.a('string')
            expect(body).to.have.property('expirationDate').that.is.a('string')
            expect(body).to.have.property('logoNumber').that.is.a('string')
            expect(body).to.have.property('activeFlag').that.is.a('string')
            expect(body).to.have.property('blockDate').that.is.a('string')
            expect(body).to.have.property('activationDate').that.is.a('string')
            expect(body).to.have.property('issueDate').that.is.a('string')
            expect(body).to.have.property('serviceCode').that.is.a('string')
            expect(body).to.have.property('indVisualDeficiency').that.is.a('string')
            expect(body).to.have.property('embossing').that.is.a('string')
            expect(body).to.have.property('registrationNumber').that.is.a('string')
            expect(body).to.have.property('reasonBlockingDescription').that.is.a('string')
            expect(body).to.have.property('numAR').that.is.a('string')
            expect(body).to.have.property('digitAR').that.is.a('string')
            expect(body).to.have.property('blockCode').that.is.a('string')
          })
        })
      })
    })
  })
})