import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Limites', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação dos limites do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${apiCard}/card-api/v5/cards/${uuid}/limits`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos
            expect(body).to.have.property('creditLimitCashNacAvailable').that.is.a('number')
            expect(body).to.have.property('creditLimitCashInter').that.is.a('number')
            expect(body).to.have.property('creditLimitNacAvailable').that.is.a('number')
            expect(body).to.have.property('creditLimitCashNac').that.is.a('number')
            expect(body).to.have.property('creditLimitInterAvailable').that.is.a('number')
            expect(body).to.have.property('creditLimitInter').that.is.a('number')
            expect(body).to.have.property('creditLimitCashInterAvailable').that.is.a('number')
            expect(body).to.have.property('creditLimitNac').that.is.a('number')
            expect(body).to.have.property('code').that.is.a('string')
            expect(body).to.have.property('description').that.is.a('string')
          })
        })
      })
    })
  })
})