import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Informações do cartão', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Obtendo informações do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${apiCard}/card-api/v2/cards/${uuid}/info`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos de nível superior
            expect(body).to.have.property('uuid').that.is.a('string')
            expect(body).to.have.property('embossName').that.is.a('string')
            expect(body).to.have.property('expirationDate').that.is.a('string')
            expect(body).to.have.property('last4Digits').that.is.a('string')
            expect(body).to.have.property('logoNumber').that.is.a('string')
            expect(body).to.have.property('status').that.is.a('string')
            expect(body).to.have.property('creditLimitNacAvailable').that.is.a('number')
            expect(body).to.have.property('creditLimitNac').that.is.a('number')
            expect(body).to.have.property('transactions').that.is.an('array')

            // Validações para cada item no array 'transactions'
            body.transactions.forEach((transaction) => {
              expect(transaction).to.have.property('date').that.is.a('string')
              expect(transaction).to.have.property('description').that.is.a('string')
              expect(transaction).to.have.property('numParcelUpTo').that.is.a('string')
              expect(transaction).to.have.property('numParcelOf').that.is.a('string')
              expect(transaction).to.have.property('typetransaction').that.is.a('string')
              expect(transaction).to.have.property('valueTransactionNational').that.is.a('string')
              expect(transaction).to.have.property('value').that.is.a('number')
            })
          })
        })
      })
    })
  })
})