import 'cypress-plugin-api'

const apiCard = Cypress.env('apiCard')

describe('Due Date', () => {

  beforeEach(() => {
    cy.mettacard_anonymous_token()
    cy.mettacard_signin_unico()
    cy.mettacard_uuid_CPF()
  })

  it('Validação da Due Date do cartão', () => {

    cy.task('getUuid').then((uuid) => {
      cy.task('getLoggedToken').then((loggedToken) => {
        cy.task('getLast4Digits').then((last4Digits) => {
          cy.api({
            method: 'GET',
            url: `${apiCard}/card-api/v1/cards/${uuid}/invoice/duedate`,
            failOnStatusCode: false,
            headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'PostmanRuntime/7.39.0',
              'Accept': '*/*',
              //'Accept-Encoding': 'gzip, deflate, br',
              'Connection': 'keep-alive',
              'Authorization': `Bearer ${loggedToken}`, // Usa o token armazenado
              'last4Digits': last4Digits
            },
          }).then((response) => {
            expect(response.status).to.eq(200)

            const body = response.body

            // Validações dos campos de nível superior
            expect(body).to.have.property('code').that.is.a('string')
            expect(body).to.have.property('description').that.is.a('string')
            expect(body).to.have.property('listInvoiceDueDate').that.is.an('array')

            // Validações para cada item no array 'listInvoiceDueDate'
            body.listInvoiceDueDate.forEach((invoice) => {
              expect(invoice).to.have.property('invoiceDueDate').that.is.a('string')
            })
          })
        })
      })
    })
  })
})