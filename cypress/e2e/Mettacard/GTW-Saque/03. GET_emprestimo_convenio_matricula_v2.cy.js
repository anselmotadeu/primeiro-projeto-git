import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Empréstimo', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      cy.mettacard_anonymous_token()
      cy.mettacard_signin_unico()
    })
  })

  it('Consultar empréstimo por convênio e matrícula', () => {
    cy.task('getLoggedToken').then((loggedToken) => {
      cy.api({
        method: 'GET',
        url: `${mettaCard}/gtw-saque-api/v2/emprestimos/convenio/165/matricula/12345678`, 
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          //'Accept-Encoding': 'gzip, deflate, br',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
        }
      }).then((response) => {
        expect(response.status).to.eq(200)
      
        const body = response.body
      
        // Validações dos campos de nível superior
        expect(body).to.have.property('emprestimos').that.is.an('array')
        expect(body).to.have.property('cpf').that.is.a('string')
        expect(body).to.have.property('dataRef').that.is.a('string')
        expect(body).to.have.property('code').that.is.a('string')
        expect(body).to.have.property('description').that.is.a('string')
      
        // Validações de valores específicos
        expect(body.cpf).to.eq('418.522.713-29')
        expect(body.code).to.eq('0')
        expect(body.description).to.eq('OK')
      })
    })      
  })
})
