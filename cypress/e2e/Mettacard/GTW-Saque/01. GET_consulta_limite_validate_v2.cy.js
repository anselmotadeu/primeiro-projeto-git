import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Limite', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      cy.mettacard_anonymous_token()
      cy.mettacard_signin_unico()
    })
  })

  it('Consulta de limite por Matrícula', () => {
    cy.task('getLoggedToken').then((loggedToken) => {
      cy.api({
        method: 'GET',
        url: `${mettaCard}/gtw-saque-api/v2/limites/convenio/165/matricula/12345678/validate?salarioLiquido=10000`, 
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          //'Accept-Encoding': 'gzip, deflate, br',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
        }
      }).then((response) => {
        console.log(response.body)
        expect(response.status).to.eq(200)

        const body = response.body

        // Validações dos campos de nível superior
        expect(body).to.have.property('cpf').that.is.a('string')
        expect(body).to.have.property('nome').that.is.a('string')
        expect(body).to.have.property('idConvenio').that.is.a('string')
        expect(body).to.have.property('matricula').that.is.a('string')
        expect(body).to.have.property('vlMultiploSaque').that.is.a('number')
        expect(body).to.have.property('limiteUtilizado').that.is.a('number')
        expect(body).to.have.property('limiteTotal').that.is.a('number')
        expect(body).to.have.property('limiteDisponivel').that.is.a('number')
        expect(body).to.have.property('vlLimiteParcela').that.is.a('number')
        expect(body).to.have.property('limiteParcelaDisponivel').that.is.a('number')
        expect(body).to.have.property('vlMultiploCompra').that.is.a('number')
        expect(body).to.have.property('vlLimiteCompra').that.is.a('number')
        expect(body).to.have.property('opcoes').that.is.an('array')
        expect(body).to.have.property('code').that.is.a('string')
        expect(body).to.have.property('description').that.is.a('string')

        // Validações dos campos específicos de valores
        expect(body.cpf).to.eq('418.522.713-29')
        expect(body.nome).to.eq('CLIENTE TESTE METTACARD UM')
        expect(body.idConvenio).to.eq('165')
        expect(body.matricula).to.eq('12345678')
        expect(body.code).to.eq('0')
        expect(body.description).to.eq('OK')

        // Validações para cada item no array 'opcoes'
        body.opcoes.forEach((opcao) => {
          expect(opcao).to.have.property('qtdParcelas').that.is.a('number')
          expect(opcao).to.have.property('vlLimiteParcela').that.is.a('number')
          expect(opcao).to.have.property('vlLimite').that.is.a('number')
        })

        // Validações específicas dos valores dentro do array 'opcoes'
        expect(body.opcoes[0]).to.have.property('qtdParcelas', 96)
        expect(body.opcoes[0]).to.have.property('vlLimiteParcela', 9000)
        expect(body.opcoes[0]).to.have.property('vlLimite', 1899050)
        expect(body.opcoes[1]).to.have.property('qtdParcelas', 84)
        expect(body.opcoes[1]).to.have.property('vlLimiteParcela', 9000)
        expect(body.opcoes[1]).to.have.property('vlLimite', 1519240)
      })
    })
  })
})
