import 'cypress-plugin-api'

const vitalPrime = Cypress.env('vitalPrime')

describe('Fourdigits signin V1', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      cy.mettacard_anonymous_token()
    })
  })

  it('Login com acesso bloqueado, pendente de habilitar o celular e login com sucesso', () => {
    cy.task('getAnonymousToken').then((anonymousToken) => {
      console.log('Anonymous Token:', anonymousToken)
      cy.api({
        method: 'POST',
        url: `${vitalPrime}/vital-prime-api/v1/fourdigits/signin`, 
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
        },
        body: {
          'cpfCnpj': otherUserCreds.document,
          'clientId': otherUserCreds.clientId, // Usa as credenciais do outro usuário
          'password': otherUserCreds.pwd,
        }
      }).then((response) => {
        expect(response.status).to.eq(400)
      
        const body = response.body
      
        // Validações dos campos de nível superior
        expect(body).to.have.property('message').that.is.a('string')
        expect(body).to.have.property('status').that.is.a('number')
      
        // Validações de valores específicos
        expect(body.message).to.eq('Documento inválido')
        expect(body.status).to.eq(400)
      })
    })
  })
})