import 'cypress-plugin-api'

const vitalPrime = Cypress.env('vitalPrime')

describe('Signin Check V3', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      cy.mettacard_anonymous_token()
    })
  })

  it('Validação do login', () => {
    cy.task('getAnonymousToken').then((anonymousToken) => {
      cy.api({
        method: 'POST',
        url: `${vitalPrime}/vital-prime-api/v3/signin/check`, failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          //'Accept-Encoding': 'gzip, deflate, br',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
        },
        body: {
          'clientId': otherUserCreds.clientId, // Usa as credenciais do outro usuário
          'document': otherUserCreds.document
        }
      }).then((response) => {
        expect(response.status).to.eq(200)

        const body = response.body

        // Validações dos campos de nível superior
        expect(body).to.have.property('action').that.is.a('string')

        // Validações de valores específicos
        expect(body.action).to.eq('SIGNIN')
      })
    })
  })
})