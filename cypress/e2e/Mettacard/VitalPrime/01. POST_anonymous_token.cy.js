import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Geração de Token Anônimo', () => {

  let creds

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      creds = data.hom.mettacard.anonymousToken // Acessa diretamente o token do mfacil
    })
  })

  it('Gerando um token anônimo', () => {
    cy.api({
      method: 'POST',
      url: `${mettaCard}/token/v2/anonymous`,
      failOnStatusCode: false,
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': 'PostmanRuntime/7.38.0',
        'Accept': '*/*',
        'Connection': 'keep-alive'
      },
      body: {
        'clientId': creds.clientId,
        'username': creds.us,
        'password': creds.pwd
      }
    }).then((response) => {
      expect(response.status).to.eq(200) // Verifica se o status da resposta é 200
      expect(response.headers['content-type']).to.include('application/json') // Verifica se o Content-Type da resposta inclui 'application/json'

      // Validações adicionais para a resposta do token
      expect(response.body).to.have.property('access_token') // Verifica se a resposta contém o campo 'access_token'
      expect(response.body.access_token).to.be.a('string') // Verifica se o 'access_token' é uma string

      expect(response.body).to.have.property('token_type') // Verifica se a resposta contém o campo 'token_type'
      expect(response.body.token_type).to.eq('Bearer') // Verifica se o valor de 'token_type' é 'Bearer'

      expect(response.body).to.have.property('expires_in') // Verifica se a resposta contém o campo 'expires_in'
      expect(response.body.expires_in).to.be.a('number') // Verifica se 'expires_in' é um número
      expect(response.body.expires_in).to.be.within(1, 3600) // Verifica se 'expires_in' está dentro de um intervalo esperado (por exemplo, entre 1 e 3600 segundos)

      // Armazena o token na variável global para uso posterior
      cy.task('setAnonymousToken', response.body.access_token)
    })
  })
})