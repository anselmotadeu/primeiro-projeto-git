import 'cypress-plugin-api'

const vitalPrime = Cypress.env('vitalPrime')

describe('Internal signin V1', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      cy.mettacard_anonymous_token()
    })
  })

  it('Login com acesso bloqueado, pendente de habilitar o celular e login com sucesso', () => {
    cy.task('getAnonymousToken').then((anonymousToken) => {
      console.log('Anonymous Token:', anonymousToken)
      cy.api({
        method: 'POST',
        url: `${vitalPrime}/vital-prime-api/v1/internal/signin`, 
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
        },
        body: {
          'document': otherUserCreds.document,
          'clientId': otherUserCreds.clientId, // Usa as credenciais do outro usuário
          'password': otherUserCreds.pwd,
        }
      }).then((response) => {
        console.log('Response Status:', response.status)
        console.log('Response Body:', response.body)

        if (response.status === 200) {
          const body = response.body
          // Validações dos campos de nível superior
          expect(body).to.have.property('message').that.is.a('string')
          // Validações de valores específicos
          expect(body.message).to.eq('OK')
        } else {
          console.error('Error Response:', response)
          // Verifica se a resposta contém a mensagem esperada
          expect(response.body).to.have.property('message').that.is.a('string')
          expect(response.body.message).to.eq('Not Found')
        }
      })
    })
  })
})