// cypress/support/commands.js

Cypress.Commands.add('mettacard_anonymous_token', () => {
  cy.log('Executing test to generate anonymous token...')

  let creds

  // Carregando dados de configuração
  cy.readFile('cypress/config/data-mass.json').then((data) => {
    creds = data.hom.mettacard.anonymousToken // Acessa diretamente o token do mettacard

    cy.api({
      method: 'POST',
      url: `${Cypress.env('mettaCard')}/token/v2/anonymous`,
      failOnStatusCode: false,
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': 'PostmanRuntime/7.38.0',
        'Accept': '*/*',
        'Connection': 'keep-alive'
      },
      body: {
        'clientId': creds.clientId,
        'username': creds.us,
        'password': creds.pwd
      }
    }).then((response) => {
      expect(response.status).to.eq(200) // Verifica se o status da resposta é 200

      // Armazena o token na variável global para uso posterior
      cy.task('setAnonymousToken', response.body.access_token)
    })
  })
})

Cypress.Commands.add('mettacard_signin_unico', () => {
  cy.log('Executing test to sign in and generate logged token...')

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  cy.readFile('cypress/config/data-mass.json').then((data) => {
    otherUserCreds = data.hom.mettacard.outroUsuario

    cy.task('getAnonymousToken').then((anonymousToken) => {
      cy.api({
        method: 'POST',
        url: `${Cypress.env('mettaCard')}/vital-prime-api/v1/signin/unico`,
        failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
        },
        body: {
          'clientId': otherUserCreds.clientId,
          'document': otherUserCreds.document,
          'password': otherUserCreds.pwd,
          'deviceId': otherUserCreds.deviceId
        }
      }).then((response) => {
        console.log(response.body)
        if (response.body.accessToken) {
          // Armazena o token de login bem-sucedido usando uma tarefa Cypress
          cy.task('setLoggedToken', response.body.accessToken)
        }
      })
    })
  })
})

Cypress.Commands.add('mettacard_uuid_CPF', () => {
  cy.log('Executing test to get UUID from CPF...')
  const cpf = '41852271329'
  cy.task('getLoggedToken').then((loggedToken) => {
    cy.api({
      method: 'GET',
      url: `https://hml-api-card.bancomaxima.com.br/card-api/v5/cards/${cpf}`,
      failOnStatusCode: false,
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': 'PostmanRuntime/7.39.0',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'Authorization': `Bearer ${loggedToken}` // Usa o token armazenado
      },
    }).then((response) => {
      // Armazena o UUID e last4Digits na variável global para uso posterior
      cy.task('setUuid', response.body.cardsResult[0].uuid)
      cy.task('setLast4Digits', response.body.cardsResult[0].last4Digits)
    })
  })
})
