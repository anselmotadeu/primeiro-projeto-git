<IMG  src="https://upload.wikimedia.org/wikipedia/pt/8/80/Banco_Master_Logo.png"  alt="Ficheiro:Banco Master Logo.png – Wikipédia, a enciclopédia livre"/>

# Automação de Testes das APIs do Master Bank

Este projeto é uma suíte de testes automatizados das APIs do Master Bank. Foi criado e é mantido pela equipe de garantia de qualidade da NAVA.

## Tabela de Conteúdos

- [Ferramentas Utilizadas](#ferramentas-utilizadas)
- [Configuração do Projeto](#configuração-do-projeto)
  - [Clonando o Repositório](#clonando-o-repositório)
  - [Instalando as Dependências](#instalando-as-dependências)
- [Executando os Testes](#executando-os-testes)
  - [Modo Interface Gráfica](#modo-interface-gráfica)
  - [Modo Headless](#modo-headless)
- [Exemplo de Código](#exemplo-de-código)
  - [Script para Gerar um Token Anônimo](#script-para-gerar-um-token-anônimo)
  - [Script para Gerar um Token Logado](#script-para-gerar-um-token-logado)
- [Modelo de Escrita dos Testes](#modelo-de-escrita-dos-testes)
- [Pipelines de Qualidade](#pipelines-de-qualidade)
- [Dashboards de Qualidade](#dashboards-de-qualidade)
- [Regras sobre o Projeto](#regras-sobre-o-projeto)
- [Configurações de Boas Práticas ESLint](#configurações-de-boas-práticas-eslint)
- [Configurações de Boas Práticas Prettier](#configurações-de-boas-práticas-prettier)
- [Propriedade](#propriedade)

## Ferramentas Utilizadas

Este projeto utiliza as seguintes ferramentas:

- [**Visual Studio Code**](https://code.visualstudio.com/Download)
- [**Cypress 13 ou superior**](https://www.cypress.io/)
- [**Node.js 18 ou superior**](https://nodejs.org/en)
- [**ESLint**](https://www.npmjs.com/package/eslint-plugin-cypress)

## Configuração do Projeto

Para configurar o projeto em sua máquina local, siga estas etapas:

1. Clone o repositório:

    ```bash
    git clone INSERIR_AQUI_O_REPO_DO_PROJETO
    ```

2. Navegue até o diretório do projeto:

    ```bash
    cd API_Automation_Cypress_Master_Bank
    ```

3. Instale as dependências do projeto:

    ```bash
    npm install
    ```

## Executando os Testes

Este projeto suporta a execução de testes em diferentes ambientes. Para executar os testes no ambiente de homologação através da interface gráfica do Cypress, você pode usar o seguinte comando:

`npx cypress open --env ENVIRONMENT=hom` ou `npm run open`

O comando acima, foi customizado no arquivo `package.json`, assim como está passivo a ser configurado mais comandos conforme a necessidade.

Para executar os testes no ambiente de homologação através da linha de comando, você pode usar o seguinte comando:

`npx cypress run --env ENVIRONMENT=hom` ou `npm run run`

## **Exemplo de código**

**Script para gerar um token anônimo**

```Javascript
import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Geração de Token Anônimo', () => {

  let creds

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      creds = data.hom.mettacard.anonymousToken
    })
  })

  it('Gerando um token anônimo', () => {
    cy.api({
      method: 'POST',
      url: `${mettaCard}/token/v2/anonymous`,
      failOnStatusCode: false,
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': 'PostmanRuntime/7.38.0',
        'Accept': '*/*',
        'Connection': 'keep-alive'
      },
      body: {
        'clientId': creds.clientId,
        'username': creds.us,
        'password': creds.pwd
      }
    }).then((response) => {
      expect(response.status).to.eq(200) // Verifica se o status da resposta é 200
      expect(response.headers['content-type']).to.include('application/json') // Verifica se o Content-Type da resposta inclui 'application/json'

      // Validações adicionais para a resposta do token
      expect(response.body).to.have.property('access_token') // Verifica se a resposta contém o campo 'access_token'
      expect(response.body.access_token).to.be.a('string') // Verifica se o 'access_token' é uma string

      expect(response.body).to.have.property('token_type') // Verifica se a resposta contém o campo 'token_type'
      expect(response.body.token_type).to.eq('Bearer') // Verifica se o valor de 'token_type' é 'Bearer'

      expect(response.body).to.have.property('expires_in') // Verifica se a resposta contém o campo 'expires_in'
      expect(response.body.expires_in).to.be.a('number') // Verifica se 'expires_in' é um número
      expect(response.body.expires_in).to.be.within(1, 3600) // Verifica se 'expires_in' está dentro de um intervalo esperado (por exemplo, entre 1 e 3600 segundos)

      // Armazena o token na variável global para uso posterior
      cy.task('setAnonymousToken', response.body.access_token)
    })
  })
})
```

**Script para gerar um token logado**

```Javascript
import 'cypress-plugin-api'

const mettaCard = Cypress.env('mettaCard')

describe('Signin', () => {

  let otherUserCreds // Variável para armazenar as credenciais do outro usuário

  beforeEach(() => {
    cy.readFile('cypress/config/data-mass.json').then((data) => {
      otherUserCreds = data.hom.mettacard.outroUsuario
      cy.mettacard_anonymous_token()
    })
  })

  it('Login com acesso bloqueado, pendente de habilitar o celular e login com sucesso', () => {
    cy.task('getAnonymousToken').then((anonymousToken) => {
      cy.api({
        method: 'POST',
        url: `${mettaCard}/vital-prime-api/v1/signin/unico`, failOnStatusCode: false,
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'PostmanRuntime/7.38.0',
          'Accept': '*/*',
          //'Accept-Encoding': 'gzip, deflate, br',
          'Connection': 'keep-alive',
          'Authorization': `Bearer ${anonymousToken}` // Usa o token armazenado
        },
        body: {
          'clientId': otherUserCreds.clientId, // Usa as credenciais do outro usuário
          'document': otherUserCreds.document,
          'password': otherUserCreds.pwd,
          'deviceId': otherUserCreds.deviceId
        }
      }).then((response) => {
        console.log(response.body)
        expect(response.status).to.eq(200)
        // Verifica se o acesso está bloqueado
        if (response.body.action === 'MAX_SIGNIN_ATTEMPTS') {
          expect(response.body).to.have.property('message', 'Atenção, acesso bloqueado por exceder tentativas permitidas com a senha incorreta. Por favor, crie uma nova senha.')
          expect(response.body).to.have.property('expiresIn', 0)
        } else if (response.body.action === 'UNKNOWN_DEVICE') {
          // Validação para quando o dispositivo é desconhecido
          expect(response.body).to.have.property('sessionId')
          expect(response.body).to.have.property('uuid')
          expect(response.body).to.have.property('message', 'Para sua segurança, por favor habilite seu celular para continuar a usar nosso aplicativo.')
          expect(response.body).to.have.property('expiresIn', 0)
        } else if (response.body.action === 'ACCOUNT_REPROVED') {
          // Validação para quando o cadastro não é aprovado
          expect(response.body).to.have.property('message', 'O seu cadastro não foi aprovado.\nAgradecemos seu interesse.')
          expect(response.body).to.have.property('expiresIn', 0)
        } else if (response.body.action === 'HOME') {
          // Validação para quando o usuário é direcionado para a HOME
          expect(response.body).to.have.property('accessToken')
          expect(response.body).to.have.property('expiresIn')
          expect(response.body).to.have.property('tokenType', 'Bearer')
        }
        // A linha abaixo só deve ser executada se o login for bem-sucedido
        if (response.body.accessToken) {
          cy.task('setLoggedToken', response.body.accessToken)
        }
      })
    })
  })
})
```
# **Modelo de escrita dos testes**

[**pageObject**](https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions)

# **Pipelines de Qualidade (em criação)**

<a href="">**Pipeline pendente de criação**</a>

# **Dashboards de Qualidade (em criação)**

<a href="">**Dashboard pendente de criação**</a>

## **Regras sobre o Projeto (Pipeline)**
Todos os testes devem ser executados tanto na **Interface Gráfica** do Cypress como também no modo **Headless** antes de subir os testes para o repositório do projeto.

Não trabalhe diretamente na `branch Develop`, pois isso pode gerar conflito de código.

Crie uma `branch` nova e depois faça um PR para a `branch Develop`.

Após fazer um `merge` da sua nova `branch`com a `branch Develop`, devemos então criar um PR para a `branch Main`.

**OBS: Todo PR deve passar pelo Code Review do Tech Quality Lead.**

### Interface Gráfica do Cypress
![image.png](/.attachments/image-cb1f071f-7eff-443f-b7e3-451cfc755511.png)

### Exemplo de um teste no modo Headless do Cypress
![image.png](/.attachments/image-0df72a99-61a7-4817-a9b2-982a8235f887.png)

#Configurações de Boas Práticas ESLint

Para garantir a qualidade e consistência do código, utilizamos o ESLint com as seguintes configurações:

```javascript
module. Exports = {
    parserOptions: {
      ecmaVersion: 2018, // Permite a análise de modernas funcionalidades do ECMAScript
      sourceType: 'module', // Permite o uso de imports
      ecmaFeatures: {
        jsx: true, // Permite a análise de JSX
      },
    },
    env: {
      'cypress/globals': true, // Define as variáveis globais do Cypress
      node: true, // Define as variáveis globais do Node.js
      es6: true, // Define as variáveis globais do ECMAScript 6
    },
    extends: [
      'plugin:cypress/recommended', // Usa as recomendações do plugin do Cypress
      'eslint:recommended', // Usa as recomendações padrão do ESLint
      'prettier' // Usa as recomendações do Prettier para o ESLint
    ],
    plugins: [
      'cypress', // Inclui o plugin do Cypress
    ],
    rules: {
      'no-unused-vars': 'error', // Proíbe variáveis não utilizadas
      'no-unused-vars': 'warn', // Avisa sobre variáveis não utilizadas
      'no-console': 'off', // Permite o uso de console.log
      "indent": ["error", 2], // Define o estilo de indentação
      "quotes": ["error", "single"], // Define aspas simples
      "semi": ["error", "never"], // Define ponto e vírgula no final de cada instrução
      "space-before-blocks": ["error", "always"], // Define espaço antes de blocos
      "brace-style": ["error", "1tbs"], // Define o estilo de chaves
      "camelcase": ["error", {"properties": "always"}], // Define camelCase
      "no-var": "error", // Proíbe o uso de var
      "complexity": ["error", 10], // Define a complexidade máxima do código
    //   "max-len": ["error", {"code": 80}], // Define o comprimento máximo de uma linha
      "no-trailing-spaces": "error", // Proíbe espaços em branco no final de uma linha
    },
  };
```

Configuramos o comando `npm lint` no arquivo `package.json` para facilitar a validação das boas práticas configuradas no arquivo `.eslintrc.js`, conforme demonstrado acima.
- basta executar o comando `npm run lint` e o `eslint` rodará em todos os arquivos JS do projeto e identificará se estamos violando alguma boa prática antes configurada.

#Configurações de Boas Práticas Prettier

Configuramos o comando `format` no arquivo `package.json` para facilitar a indentação do projeto e seguir as boas práticas estabelecidas.
- Basta executar o comando `npm run format` e todos os arquivos de testes serão indentados corretamente.

## Propriedade

- Este projeto foi arquitetado, estruturado e produzido pela equipe de qualidade da NAVA. Todos os direitos reservados.