module.exports = {
    parserOptions: {
      ecmaVersion: 2018, // Permite a análise de modernas funcionalidades do ECMAScript
      sourceType: 'module', // Permite o uso de imports
      ecmaFeatures: {
        jsx: true, // Permite a análise de JSX
      },
    },
    env: {
      'cypress/globals': true, // Define as variáveis globais do Cypress
      node: true, // Define as variáveis globais do Node.js
      es6: true, // Define as variáveis globais do ECMAScript 6
    },
    extends: [
      'plugin:cypress/recommended', // Usa as recomendações do plugin do Cypress
      'eslint:recommended', // Usa as recomendações padrão do ESLint
      'prettier' // Usa as recomendações do Prettier para o ESLint
    ],
    plugins: [
      'cypress', // Inclui o plugin do Cypress
    ],
    rules: {
      'no-unused-vars': 'error', // Proíbe variáveis não utilizadas
      'no-unused-vars': 'warn', // Avisa sobre variáveis não utilizadas
      'no-console': 'off', // Permite o uso de console.log
      "indent": ["error", 2], // Define o estilo de indentação
      "quotes": ["error", "single"], // Define aspas simples
      "semi": ["error", "never"], // Define ponto e vírgula no final de cada instrução
      "space-before-blocks": ["error", "always"], // Define espaço antes de blocos
      "brace-style": ["error", "1tbs"], // Define o estilo de chaves
      "camelcase": ["error", {"properties": "always"}], // Define camelCase
      "no-var": "error", // Proíbe o uso de var
      "complexity": ["error", 10], // Define a complexidade máxima do código
    //   "max-len": ["error", {"code": 80}], // Define o comprimento máximo de uma linha
      "no-trailing-spaces": "error", // Proíbe espaços em branco no final de uma linha
    },
  };  