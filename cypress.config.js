const { readFileSync } = require('fs')

module.exports = {
  e2e: {
    env: {
      credCesta: 'https://hml-api-gtw-credcestaconsig.bancomaxima.com.br',
      clientId: '5iqcj7tq199ql193713cdt82s4',
      username: '9591b6cd-839f-421b-a624-668ff4eab7df',
      password: 'TVNu445@9QY2a63y',
      mFacil: "https://hml-api-gtw-masterconsig.bancomaxima.com.br",
      mettaCard: "https://hml-api-gtw-mettacard.bancomaxima.com.br",
      apiCard: "https://hml-api-card.bancomaxima.com.br",
      vitalPrime: "https://hml-api-vitalprime.bancomaxima.com.br",
    },
    experimentalRunAllSpecs: true,
    retries: 1,
    setupNodeEvents(on, config) {
      // implement node event listeners here
      const envName = config.env.ENVIRONMENT
      const text = readFileSync(`./cypress/config/${envName}.config.json`)
      const values = JSON.parse(text)
      config.env = {
        ...values,
      }

      on('task', {
        log(message) {
          if (envName != 'prd')
            console.log(message)
          return null
        },
        logValue({ message, value = null }) {
          if (envName != 'prd')
            console.log(message, value ?? '')
          return null
        },
        setAnonymousToken (token) {
          process.env.anonymousToken = token
          return null
        },
        getAnonymousToken () {
          return process.env.anonymousToken
        },
        setLoggedToken (token) {
          process.env.loggedToken = token
          return null
        },
        getLoggedToken () {
          return process.env.loggedToken
        },
        setUuid (uuid) {
          process.env.uuid = uuid
          return null
        },
        getUuid () {
          return process.env.uuid
        },
        setLast4Digits (last4Digits) {
          process.env.last4Digits = last4Digits
          return null
        },
        getLast4Digits () {
          return process.env.last4Digits
        },
        video: true, // desabilita a gravação de vídeos
    env: {
      ENVIRONMENT: "blab",
      "credcesta": "https://hml-api-gtw-credcestaconsig.bancomaxima.com.br",
      "mFacil": "https://hml-api-gtw-masterconsig.bancomaxima.com.br",
      "mettaCard": "https://hml-api-gtw-mettacard.bancomaxima.com.br",
      "apiCard": "https://hml-api-card.bancomaxima.com.br",
      "vitalPrime": "https://hml-api-vitalprime.bancomaxima.com.br"
    },
      })
    },
  },
}